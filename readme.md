# Ferramentas desenvolvimento front-end

**Gulp**

        npm install --global gulp gulp-cli

**Tasks**

        gulp // inicia o gulp dev e o styl
        gulp dev // inicia watch nos arquivos styl
        gulp styl // gera os arquivos css a partir de arquivos stylus

**Stylus**

    - https://www.npmjs.com/package/gulp-stylus

**Bootstrap stylus**

    - https://github.com/maxmx/bootstrap-stylus
    - http://getbootstrap.com/


